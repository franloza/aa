
function [result] = mcint(fun, a, b, num_puntos) 	
	
	#Set the timer
	tic();
	
	#Set the distance between the points of the X axis	
	xDistance= 0.1;
	vXaxis = [a:xDistance:b];

	#Evaluates the points of the X axis and calculates it maximum
	vYaxis = fun(vXaxis);
	maximum = max(vYaxis);	

	#Calculates the random numbers for the X axis
	vPrecision = (b-a)/(num_puntos-1);
	vRandomX = [a:vPrecision:b];

	#Calculates the random numbers for the Y axis
	vRandomY = rand(1,num_puntos);
	vRandomY = vRandomY .* maximum ;

	#Evaluate the points of the random coordinates
	vEvaluatedPoints = fun(vRandomX);
	vAciertos = vRandomY <= vEvaluatedPoints;
	aciertos = length(find(vAciertos==1));

	#Calculates the result using Monte Carlo formula
	result = (aciertos/num_puntos)* (b-a) * maximum;
	
	#Stop the timer
	timeEllapsed = toc();

	#Draw the graphics 
	plot(vXaxis,vYaxis,"linewidth",2,
		 vRandomX,vRandomY,"x","color","red","markersize",6);

	#Display the results (Comment when using precisionFunction)
	#fprintf("Aproximated value using Monte Carlo method: %f\n", result);
	#fprintf("Real value using quad() function: %f\n",quad(fun,a,b));
	#fprintf("Time ellapsed: %f\n",timeEllapsed);
endfunction

	


	


