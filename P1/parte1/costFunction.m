function [result] = costFunction(theta,x,y)
	m = length(y);

	result = (1 / (2*m)) * sum((hFunction(x,theta)-y).^2);
endfunction
