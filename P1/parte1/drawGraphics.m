function void = drawGraphics(xVector, yVector, thetaVector)
	theta0RangeStart = -10;
	theta0RangeEnd = 10;
	theta1RangeStart = -1;
	theta1RangeEnd = 4;
	nPoints = 150;
	
	precision = (length(theta0RangeStart:theta0RangeEnd) - 1) / (nPoints - 1);
	theta0Range = [theta0RangeStart:precision:theta0RangeEnd];
	precision = (length(theta1RangeStart:theta1RangeEnd - 1)) / (nPoints - 1);
	theta1Range = [theta1RangeStart:precision:theta1RangeEnd];
	thetaRangeVector = ([theta0Range;theta1Range]);
	totalCost = wholeCost(thetaRangeVector, xVector, yVector);
	[useless, thetas] = descentGradient(xVector, yVector, 0.0025);

	# Surface plot
	figure(2);
	surface(theta0Range, theta1Range, totalCost);
	hold on;
	z = costFunction(thetas, xVector, yVector);
	plot3(thetas(1,:), thetas(2,:), z, "markersize", 1, "marker", "*", "color", "m", "linewidth", 3, "markeredgecolor", "r");
	ax = gca();
	set(ax, "xgrid", "on", "ygrid", "on", "zgrid", "on", "gridlinestyle", "-");
	title("Cost Surface", "fontsize", 25);
	xlabel("θ₀", "fontsize", 15);
	ylabel("θ₁", "fontsize", 15);
	
	# Contour plot
	figure(3);
	contour(theta0Range, theta1Range, totalCost, logspace(-2, 3, 20), "linewidth", 1);
	hold on;
	plot(thetas(1,:), thetas(2,:), "color", "red", "marker", "x", "markersize", 6, "linewidth", 1);
	title("Countour", "fontsize", 25);
	xlabel("θ₀", "fontsize", 15);
	ylabel("θ₁", "fontsize", 15);
endfunction

function [costVector] = wholeCost(thetaVector, xVector, yVector)
	n = length(thetaVector);
	m = length(xVector);

	for (i = [1:n])
		for (j = [1:n])
			costVector(j, i) = (1 / (2*m)) * sum((hypothesis(xVector,thetaVector(1, i), thetaVector(2, j)) - yVector) .^ 2);
		endfor
	endfor
endfunction

function [prediction] = hypothesis(xVector, theta0, theta1)
	prediction = (xVector * theta1) + theta0;
endfunction
