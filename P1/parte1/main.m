data = load ("ex1data1.txt");
x = data(:,1);
y = data(:,2);
m = length(y);
f = @(x,theta) (theta(1) + theta(2) .* x);

# ALPHA = 0.02 SEEMS TO WORK FINE. WITH HIGHER VALUES DIVERGUES
alpha = 0.0025;
#Test Case
prueba = 10.650;

printf("Test case for a population of 10.650 people\n");
[theta, useless] = descentGradient(x,y,alpha);
result = f(prueba,theta);
printf("Result (Profit) using descent gradient: %f\n",result);

drawGraphics(x, y,theta);
