function [newTheta, variousThetas] = descentGradient(X,y,alpha)
m = length(y);
n = length(X(1,:));
X = [ones(m,1),X];
theta = zeros(n+1,1);
newTheta = zeros(n+1,1);
j = 1;
for (i = [1:1500])
	for i = 1:n+1
		newTheta(i) = theta(i) - (alpha/m * sum ((hFunction(X,theta)-y) .*X(:,i)));
	endfor
	#if(i < 10)
		variousThetas(:,j) = theta;
		j = j + 1;
	#elseif(mod(i,10) == 0)
		#variousThetas(:,j) = theta;
		#j = j + 1;
	#endif
	theta = newTheta;
	
	#Testing
        #cost = costFunction(theta,norm_X,y);
        #disp(cost);
        #testX = [ones(m,1),norm_X];
        #disp(f(testX,theta));
        #plot(X,f(testX,theta),X,y,"x","color","red","markersize",6);
        #pause(0.1);
	
	endfor	
endfunction
