data = load ("ex1data2.txt");
n = length(data(1,:))-1;
X = data(:,1:n);
y = data(:,n+1); 
m = length(y);
f = @(x,theta) (hFunction(x,theta));

# Normalizes the data
[norm_X,mu,sigma] = normalizaAtributo(X);

# Alpha value
alpha = 0.1;

#Test Case 
prueba = [1,1650,3];
norm_prueba = normalizaAtributo(prueba);

printf("Test case for 1950m2 and 3 rooms\n");

# Descent gradient
[theta, useless] = descentGradient(norm_X,y,alpha);
result = f(norm_prueba,theta);
printf("Result (price) using descent gradient: %f\n",result);

# Normal equation
theta2 = normalEquation(norm_X,y);
result = f(norm_prueba,theta);
printf("Result (price) using normal equation: %f\n",result);
drawGraphics(norm_X, y);
