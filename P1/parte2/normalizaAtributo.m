function [X_norm,mu,sigma] = normalizaAtributo(X)
mu = mean(X);
sigma = std(X);
m = length(X(:,1));

Xsigma = repmat(mean(X),m,1);
XVariance = repmat(var(X),m,1);
X_norm = (X - Xsigma)./(Xsigma); 

endfunction
