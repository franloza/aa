data = load ("ex2data2.txt");
n = length(data(1,:))-1;
X = data(:,1:n);
y = data(:,n+1); 

#Apply extra features with the combination of X1 and X2 until degree 6
X = mapFeature(X(:,1),X(:,2));

n = length(X(1,:));
m = length(y);
initial_theta = zeros(n,1);


#Lambda value for regularization
lambda = 1;

costFunction(initial_theta,X,y,lambda)


#Optimization
options = optimset('GradObj','on','MaxIter',400);
[theta,cost] = fminunc(@(t)(costFunction(t,X,y,lambda)), initial_theta,options);

#Graphics
plotDecisionBoundary(theta,X,y);

