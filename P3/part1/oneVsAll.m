function [all_theta] = oneVsAll(X,y,num_etiquetas, lambda)
	m = length(y);
	n = length(X(1,:));

	# Adding a column of ones to X
	X = [ones(m,1),X];

	initial_theta = zeros(n + 1, 1);

	options = optimset("GradObj", "on", "MaxIter", 50);
	theta = initial_theta;
	for c = 1:num_etiquetas
		[theta] = fmincg(@(t)(lrCostFunction(t, X, (y == c), lambda)), theta, options);
		# In each iteration we add a new column to our theta
		all_theta(:,c) = theta;
	endfor
endfunction
