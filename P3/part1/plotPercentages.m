function plotPercentages(X,y,lambdaVector)

	#Computation

	for i = 1:length(lambdaVector)
		theta = oneVsAll(X,y,10,lambdaVector(i));	

		#Cache
		dlmwrite(["cache/cachedThetaL" num2str(lambdaVector(i))], theta);
		#theta = dlmread(["cache/cachedThetaL" num2str(lambdaVector(i))]);

		percentages(i) = percentageAccuracy(X, y, theta);

	endfor

	#Drawings

	figure;
	plot(lambdaVector,percentages,"linewidth", 2);
	title("Percentage against lambdas", "fontsize", 25);
	xlabel("Lambdas", "fontsize", 15);
	ylabel("Accuracy percentage(%)", "fontsize", 15);
endfunction
