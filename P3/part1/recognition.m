function etiqueta = recognition(X, theta)
	# Adding a column of ones to X
	m = length(X(:,1));
	X = [ones(m,1),X];
	
	etiqueta = hFunction(X,theta);

	# We have to transpose etiqueta because the way max works
	[useless, etiqueta] = max(etiqueta');
endfunction
