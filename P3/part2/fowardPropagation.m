function hypothesis = fowardPropagation(X,Theta1,Theta2)
	m = length(X(:,1));
	X = [ones(m,1),X];
	
	a2 = sigmoidFunction(X * Theta1');
	
	a2 = [ones(length(a2(:,1)),1),a2];
	
	a3 = sigmoidFunction(a2 * Theta2');
	
	hypothesis = a3;
endfunction
