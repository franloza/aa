function [result] = sigmoidDerivative (z)

a = sigmoidFunction(z);
a = [ones(1, columns(a)); a];

result = a .* (1 - a);

endfunction
