1;
#--------------------------------------------------------------------------
#Function that calculates the error produced over the training examples and the Validation
#examples by the increasing subsets of X and y. This allow as to analize the bias/variance
#or our training algorithm. Also returns the final trained theta for practical purposes

function [errTraining, errValidation,theta] = errEvoTraining (X,y,Xval,yval,lambda)

#Expand Training X and Validation X with a column of 1s (Independent term)
m = rows(X);
X1 = [ones(m,1),X];
mVal = rows(Xval);
Xval1 = [ones(mVal,1),Xval];

#Iterates over the increasing subsets of X and Y
for i= 1 :m
   theta = training(X(1:i,:), y(1:i) ,lambda);
   errTraining(i) = costFunction (theta,X1(1:i,:),y(1:i),0);
   errValidation(i) = costFunction (theta,Xval1,yval,0);
end

endfunction

#--------------------------------------------------------------------------
#Function similar to errEvoTraining but using p extended features
function [errTraining, errValidation,theta] = errEvoExtTraining (X,y,Xval,yval,lambda,p)

#Expand Training X and Validation X with a column of 1s (Independent term)
m = rows(X);
X1 = featureExtension(X,p);
[X1,mu,sigma] = featureNormalize(X1);
X1 = [ones(m,1),X1];

#Normalize the validation examples using the mean and the variance of the
#training examples
mVal = rows(Xval);
Xval1 = featureExtension(Xval,p);
Xval1 = bsxfun(@minus, Xval1, mu);
Xval1 = bsxfun(@rdivide, Xval1, sigma);
Xval1 = [ones(mVal,1),Xval1];

#Iterates over the increasing subsets of X and Y
for i= 1 :m
   theta = extTraining2(X(1:i,:), y(1:i) ,lambda,p,mu,sigma);
   errTraining(i) = costFunction (theta,X1(1:i,:),y(1:i),0);
   errValidation(i) = costFunction (theta,Xval1,yval,0);
end

endfunction
