1;

#Graphic 1 (Relationship regresion line - training examples)

function [] = G_LinReg(X,y,theta)

figure;
plot(X,y,"x","color",'r',"markersize",10,"Linewidth", 1);
hold on;
plot(X,prediction(X,theta),"Linewidth", 2);
hold off;

endfunction

#--------------------------------------------------------------------------

#Graphic 2 (Relationship regresion line - extended training examples)
function G_ExtLinReg(X,y,theta,mu,sigma,p)

figure;
plot(X,y,"x","color",'r',"markersize",10,"Linewidth", 1);
hold on;
plotFit(min(X), max(X), mu, sigma, theta, p);
hold off;

endfunction

#--------------------------------------------------------------------------

#Graphic 3 (Relationship between the evolution of the error over the
#training examples - validation examples)

function [] = G_ValidationCurves(X,errTraining, errValidation)

figure;
#Number of training examples
m = rows(X);
mVector = [1:m];
plot(mVector,errTraining,"color",'b',"Linewidth", 2);
hold on;
plot(mVector,errValidation,"color",'g',"Linewidth", 2);
hold off;

endfunction

#--------------------------------------------------------------------------

#Graphic 4 (Plots a learned polynomial regression fit over an existing figure)

function plotFit(min_x, max_x, mu, sigma, theta, p)

%Also works with linear regression.
%   PLOTFIT(min_x, max_x, mu, sigma, theta, p) plots the learned polynomial
%   fit with power p and feature normalization (mu, sigma).

% Hold on to the current figure
hold on;

% We plot a range slightly bigger than the min and max values to get
% an idea of how the fit will vary outside the range of the data points
x = (min_x - 15: 0.05 : max_x + 25)';

% Map the X values
X_poly = x;
for i=2:p
  X_poly = [X_poly x.^i];
end

X_poly = bsxfun(@minus, X_poly, mu);
X_poly = bsxfun(@rdivide, X_poly, sigma);

% Add ones
X_poly = [ones(size(x, 1), 1) X_poly];

% Plot
plot(x, X_poly * theta, '--', 'LineWidth', 2)

% Hold off to the current figure
hold off

end


#--------------------------------------------------------------------------

function G_lambdaAnalysis (trainingX,trainingY,validationX,validationY,p)

figure;
lambdaVector = [0,.001,.003,.01,.03,.1,.3,1,3,10];

#Extend and normalize the training examples
m = rows(trainingX);
extTrainingX = featureExtension(trainingX,p);
[extTrainingX,mu,sigma] = featureNormalize(extTrainingX);
extTrainingX = [ones(m,1),extTrainingX];

#Extend and normalize the validation examples using mu and sigma of the training
#examples
mVal = rows(validationX);
extValidationX = featureExtension(validationX,p);
extValidationX = bsxfun(@minus, extValidationX, mu);
extValidationX = bsxfun(@rdivide, extValidationX, sigma);
extValidationX = [ones(mVal,1),extValidationX];

for i= 1 :columns(lambdaVector)
  theta = extTraining(trainingX,trainingY,lambdaVector(i),p);
  errTraining(i) = costFunction (theta,extTrainingX,trainingY,0);
  errValidation(i) = costFunction (theta,extValidationX,validationY,0);
end
plot(lambdaVector,errTraining,"color",'b',"Linewidth", 2);
hold on;
plot(lambdaVector,errValidation,"color",'g',"Linewidth", 2);

endfunction
