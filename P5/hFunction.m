function [result] = hFunction(X,theta)

thetaTrans = theta';
X = thetaTrans * X';
result = X';

endfunction
