source("graphics.m");
source("training.m");
source("errEvoTraining.m");
#Load the following variables: X, Xtest, Xval, data, y, ytest and  yval
load ("ex5data1.mat");

#Set the number of columns to extend x1
p=8;

#Set value of lambda
lambda = 0;

#Training of theta
theta = training(X, y, lambda);
G_LinReg(X,y,theta);

#Training of theta adding p features
[theta,mu,sigma] = extTraining(X, y, lambda,p);
G_ExtLinReg(X,y,theta,mu,sigma,p);

#Generation of the learning curves data
[errTraining,errValidation,theta] = errEvoTraining (X,y,Xval,yval,lambda);
G_ValidationCurves(X,errTraining,errValidation);

[errTraining,errValidation,theta] = errEvoExtTraining (X,y,Xval,yval,lambda,p);
G_ValidationCurves(X,errTraining,errValidation);

#Graphic for selecting the parameter lambda

G_lambdaAnalysis (X,y,Xval,yval,p);

#Checking of the tests examples

lambda = 3;
[theta,mu,sigma] = extTraining(X, y, lambda,p);
error = checkError(Xtest,ytest,theta,mu,sigma,p);
printf("\nError of the test examples: %f\n",error);
