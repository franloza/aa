function result = prediction(X,theta)

m = length(X(:,1));
X = [ones(m,1),X];
result = hFunction(X,theta);

endfunction
