1;
source("graphics.m");

%Function for training the theta vector using the training examples and not-extended
%features.
function theta = training(X, y, lambda)
	m = rows(X);
	printf ("Number of training examples: %i\n", m);
	X = [ones(m,1),X];
	n = columns(X);
	initial_theta = ones(n,1);

	options = optimset("GradObj", "on", "MaxIter", 100);

	[theta] = fmincg(@(t)(costFunction(t, X, y, lambda)), initial_theta, options);

endfunction

%Function for training the theta vector using the training examples with a feature
%extended p times
function [theta,mu,sigma] = extTraining(X, y, lambda,p)

	#Extend the matrix and normalize it
  extX = featureExtension(X,p);
	[extX ,mu,sigma]= featureNormalize(extX);

	theta = training(extX,y,lambda);

endfunction

%Same function than extTraining but regularizing the data using a specific mu and
%sigma values
function theta = extTraining2(X, y, lambda,p,mu,sigma)

	#Extend the matrix and normalize it
  extX = featureExtension(X,p);
	m = rows(X);
	extX = bsxfun(@minus, extX, mu);
	extX = bsxfun(@rdivide, extX, sigma);

	theta = training(extX,y,lambda);

endfunction
