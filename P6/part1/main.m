# Part 1
#{
load("ex6data1.mat");
# Loads X,y

C = 1;

for i=1:20
	C = i * 5;
	model = svmTrain(X, y, C, @(x1, x2)gaussianKernel(x1, x2, sigma));
	visualizeBoundaryLinear(X, y, model);
	pause;
endfor
#}

#{
# Part 2
load("ex6data2.mat");
C = 1;
sigma = .1;

model = svmTrain(X, y, C, @(x1, x2)gaussianKernel(x1, x2, sigma));

visualizeBoundary(X, y, model);
#}

# Part 3
load("ex6data3.mat");
values = [0.01, .03, .1, .3, 1, 3, 10, 30]; 

#{
for i=1:length(values)
	for j=1:length(values)
		model = svmTrain(X, y, values(i), @(x1, x2)gaussianKernel(x1, x2, values(j)));
		hypothesis = svmPredict(model, Xval);
		accuracyMatrix(i, j) = percentageAccuracy(hypothesis, yval);
	endfor
endfor
#}

#dlmwrite("accuracyMatrix", accuracyMatrix);
accuracyMatrix = dlmread("accuracyMatrix");

[maxX, maxY] = find(max(max(accuracyMatrix)) == accuracyMatrix);

display(["Best C and sigma are: " num2str(values(maxX)) ", " num2str(values(maxY))]);

bestModel = svmTrain(X, y, values(maxX), @(x1, x2)gaussianKernel(x1, x2, values(maxY)));

visualizeBoundary(Xval, yval, bestModel);
