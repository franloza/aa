function accuracyPlot(values,accuracyMatrix)

surf(values, values, accuracyMatrix);
xlabel("Values for C");
ylabel("Values for sigma");
zlabel("Percentage of accuracy");

endfunction
