%Function that receives a m examples (X and y) and a structure with three fields
%(training,adjustment and validation), that corresponds to the three distributions
%of our training examples. Each distribution has the number of emails of each type that
%will be devoted to that distribution. The function returns two structures (dist_X and
%dist_y, that will have the training examples of each distribution). 


function [distX, disty] = distExamples(X,y,distribution)

    # Types of email in each distribution
    # 1 = spam
    # 2 = Easy Ham
    # 3 = Hard Ham

  nSpam_total = distribution.training{1} + distribution.adjustment{1} + distribution.validation{1};
  nEHam_total = distribution.training{2} + distribution.adjustment{2} + distribution.validation{2};

  spam_counter = 1;
  eHam_counter = nSpam_total + 1;
  hHam_counter = nSpam_total + nEHam_total + 1 ;

  #Training Examples
  distX.training = [X(spam_counter:spam_counter + distribution.training{1} -1,:);
  				X(eHam_counter:eHam_counter + distribution.training{2}-1,:);
  				X(hHam_counter:hHam_counter + distribution.training{3}-1,:)];

  disty.training = [y(spam_counter:spam_counter + distribution.training{1} -1,:);
  				y(eHam_counter:eHam_counter + distribution.training{2}-1,:);
  				y(hHam_counter:hHam_counter + distribution.training{3}-1,:)];

  # Adjustment Examples
  spam_counter += distribution.training{1};
  eHam_counter += distribution.training{2};
  hHam_counter += distribution.training{3};

  distX.adjustment = [X(spam_counter:spam_counter + distribution.adjustment{1} -1,:);
  				X(eHam_counter:eHam_counter + distribution.adjustment{2}-1,:);
  				X(hHam_counter:hHam_counter + distribution.adjustment{3}-1,:)];

  disty.adjustment = [y(spam_counter:spam_counter + distribution.adjustment{1} -1,:);
  				y(eHam_counter:eHam_counter + distribution.adjustment{2}-1,:);
  				y(hHam_counter:hHam_counter + distribution.adjustment{3}-1,:)];

  # Validation Examples
  spam_counter += distribution.adjustment{1};
  eHam_counter += distribution.adjustment{2};
  hHam_counter += distribution.adjustment{3};

  distX.validation = [X(spam_counter:spam_counter + distribution.validation{1} -1,:);
  				X(eHam_counter:eHam_counter + distribution.validation{2}-1,:);
  				X(hHam_counter:hHam_counter + distribution.validation{3}-1,:)];

  disty.validation = [y(spam_counter:spam_counter + distribution.validation{1} -1,:);
  				y(eHam_counter:eHam_counter + distribution.validation{2}-1,:);
  				y(hHam_counter:hHam_counter + distribution.validation{3}-1,:)];

endfunction
