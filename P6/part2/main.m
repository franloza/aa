#PROGRAM VARIABLES

#Emails directories
eHamDir = "easy_ham";
hHamDir = "hard_ham";
spamDir = "spam";

#Number of examples of Spam emails
nSpam_total	= 500;
#Number of examples of Easy Ham emails
nEHam_total = 2551;
#Number of examples of Hard Ham emails
nHHam_total	= 250;

#Distribution of examples of each purpose (Training, Adjustment or Validation)
distribution.training = {
	spam = 300;
	easy_ham = 1500;
	hard_ham = 150;
};

distribution.adjustment = {
	spam = 100;
	easy_ham = 500;
	hard_ham = 50;
};

distribution.validation = {
	spam = 100;
	easy_ham = 551;
	hard_ham = 50;
};

#---------------------------------------------------------------------------------

# We load the vocabulary Hash Table
vocabList = getVocabList();
for i=1:length(vocabList)
	vocabHash.(vocabList{i}) = i;
endfor

# Now we process each mail, thus creating X and y
emailNum = 1;
X = [];
y = [];

#{
#Spam emails
printf("Reading spam emails..."); fflush(stdout);
[X, y, emailNum] = readEmails(nSpam_total, spamDir, 1, vocabHash, emailNum, X, y);
# Easy Ham emails
printf("Reading Easy Ham emails..."); fflush(stdout);
[X, y, emailNum] = readEmails(nEHam_total, eHamDir, 0, vocabHash, emailNum, X, y);
# Hard Ham emails
printf("Reading Hard Ham emails..."); fflush(stdout);
[X, y, emailNum] = readEmails(nHHam_total, hHamDir, 0, vocabHash, emailNum, X, y);
#}

# Loads/Saves the processed emails in disk
#dlmwrite("X.txt", X);
#dlmwrite("y.txt", y);
X = dlmread("X.txt");
y = dlmread("y.txt");

# Now we distribute each example depending on the purpose

[dist_X,dist_y] = distExamples(X,y,distribution);

# Adjustment itself(Search of optimal C and sigma)

#We could assume that the best values for C and sigma are between 0.01 and 30
#values = [0.01, .03, .1, .3, 1, 3, 10, 30];
#...or between 3 and 10
values = [3,4,5,6,7,8,9,10];
#...or between 10 and 18
#values = [10,11,13,15,16,17,18];

#Prove the different combiations for the values of C and sigma
for i=1:length(values)
	for j=1:length(values)
		model = svmTrain(dist_X.training, dist_y.training, values(i), @(x1, x2)gaussianKernel(x1, x2, values(j)));
		hypothesis = svmPredict(model, dist_X.adjustment);
		accuracyMatrix(i, j) = percentageAccuracy(hypothesis, dist_y.adjustment);
 	endfor
endfor

# Loads/Saves the result of the adjustment process in disk (With values from 0.01 to 30)
#dlmwrite("accuracyMatrix.txt", accuracyMatrix);
#accuracyMatrix = dlmread("accuracyMatrix.txt")

# Loads/Saves the result of the adjustment process in disk (With values from 3 to 10)
dlmwrite("accuracyMatrix2.txt", accuracyMatrix);
accuracyMatrix = dlmread("accuracyMatrix2.txt")

# Loads/Saves the result of the adjustment process in disk (With values from 10 to 18)
#dlmwrite("accuracyMatrix3.txt", accuracyMatrix);
#accuracyMatrix = dlmread("accuracyMatrix3.txt")

#Draw a 3D graphics for the accuracy matrix
accuracyPlot(values,accuracyMatrix);

#We chose the maximum value(s) of accuracy and edist_X.trainingct the values of C and sigma for this value
maxPercentage = max(max(accuracyMatrix));

[bestC, bestSigma] = find(maxPercentage == accuracyMatrix);

# Select the fist in case two or more sigma and C have the best accuracy
bestC = bestC(1);
bestSigma = bestSigma(1);

#We edist_X.trainingct the model using the training examples and the selected values of C and sigma
bestModel = svmTrain(dist_X.training, dist_y.training, values(bestC), @(x1, x2)gaussianKernel(x1, x2, values(bestSigma)));

#Calculates the accuracy of the model predicting the validation examples
acc = percentageAccuracy(svmPredict(bestModel, dist_X.validation), dist_y.validation);

display(["The best values for C and sigma are " num2str(values(bestC)) " and " num2str(values(bestSigma)) " with an accuracy of " num2str(acc) "%"]);
display(["Accuracy of the algorithm:  " num2str(acc) "%"]);
